<h1>Projeto Jogos Indie</h1>

Projeto utlizando NextJS, com o objetivo de gerenciar informações de colaboradores, unidades e cargos de uma empresa.

<h1>Hooks utilizados:</h1>

- UseContext
- UseState
- UseEffect.

<h1>Contato</h1>

    Nome: Bruno Gomes Esteves
    Email: n3586@hotmail.com
    Linkedin: https://www.linkedin.com/in/brunogesteves/

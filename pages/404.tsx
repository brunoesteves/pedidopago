import React from 'react';
import Common from '../components/common';

import {BodyArea} from "../styles/404"

import SentimentVeryDissatisfiedOutlinedIcon from '@mui/icons-material/SentimentVeryDissatisfiedOutlined';

export default function App() {
  return (
    <Common iconBtn namePage="Página não existente">
      <BodyArea>Desculpe, Página não existente
        <br />
        <br />
            <SentimentVeryDissatisfiedOutlinedIcon color='info'/>
      </BodyArea>
    
    </Common>
  );
}

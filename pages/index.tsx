import React, { useState } from 'react';
import type { GetServerSideProps, NextPage } from 'next';
import Box from '@mui/material/Box';

import Common from '../components/common';

import { TabArea, TableContentArea } from '../styles/common';

import Employees from '../components/Employees';
import Branchs from '../components/Branch';
import Departments from '../components/Department'
import Roles from '../components/Role'

import api from '../components/api';

import { IEmployee } from '../types/employee';
import { IBranch } from '../types/branch';
import { IDepartment } from '../types/departments';
import { IRole } from '../types/roles';
import { Tabs, Typography } from '@mui/material';
interface HomeProps {
  employeesList: IEmployee[];
  branchslist: IBranch[];
  departmentslist: IDepartment[];
  roleslist: IRole[];
}

type Results = {
  value: number;
  index: number;
};

const TabPanel: NextPage<Results> = ({ children, value, index, ...other }) => {
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}>
      {value === index && (
        <Box sx={{ py: 4, pl: 0 }}>
          <Typography component={'span'}>{children}</Typography>
        </Box>
      )}
    </div>
  );
};

const Home: NextPage<HomeProps> = ({ employeesList, branchslist,departmentslist, roleslist }) => {
  const [titlePage, setTitlePage] = useState<string>('Colaboradores');
  const [value, setValue] = useState<number>(0);
  const handleChange = (event: React.SyntheticEvent<EventTarget>, newValue: number) => {
    setValue(newValue);
  };

  return (
    <Common iconBtn={false} namePage={titlePage}>
      <Box sx={{ width: '100%', paddingX: '3%', paddingTop: 4 }}>
        <TableContentArea>
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <Tabs
              value={value}
              onChange={handleChange}
              aria-label="tabs"
              TabIndicatorProps={{
                style: { background: '#22E0A1' }
              }}>
              <TabArea label="Colaboradores" onClick={() => setTitlePage('Colaboradores')} />
              <TabArea label="Filiais" onClick={() => setTitlePage('Filiais')} />
              <TabArea label="Departmanentos" onClick={() => setTitlePage('Departmanentos')} />
              <TabArea label="Cargos" onClick={() => setTitlePage('Cargos')} />
            </Tabs>
          </Box>
        </TableContentArea>
      
        <TabPanel value={value} index={0}>
          <Employees employeesList={employeesList} />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <Branchs Branchslist={branchslist} />
        </TabPanel>
        <TabPanel value={value} index={2}>
          <Departments Departmentslist={departmentslist} />
        </TabPanel>
        <TabPanel value={value} index={3}>
          <Roles Roleslist={roleslist} />
        </TabPanel>
      </Box>
    </Common>
  );
};

export default Home;

export const getServerSideProps: GetServerSideProps = async () => {
  const getAllEmployees = await api.get('/employees');
  const getAllBranchs = await api.get('/branchs');
  const getAllDepartments = await api.get('/departments');
  const getallRoles = await api.get('/roles');

  const employeesList = getAllEmployees.data;
  const branchslist = getAllBranchs.data;
  const departmentslist = getAllDepartments.data;
  const roleslist = getallRoles.data;

  return {
    props: { employeesList, branchslist,departmentslist, roleslist }
  };
};

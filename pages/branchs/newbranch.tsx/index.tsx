import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import Common from '../../../components/common';

import Link from 'next/link';

import TextField from '@mui/material/TextField';
import Alert from '@mui/material/Alert';
import Button from '@mui/material/Button';
import Close from '@mui/icons-material/Close';

import { ButtonArea, FormArea, NewBranchArea } from '../../../styles/newBranch';

import { IBranch } from '../../../types/branch';

import api from '../../../components/api';
import { Dialog } from '@mui/material';

export default function App() {
  const [OpenAlert, setOpenAlert] = useState<boolean>(false);
  const [isAdded, setIsAdded] = useState<boolean>(false);
  const [newBranchId, setNewBranchId] = useState<number>(0);

  const handleClick = () => {
    setOpenAlert(!open);
  };

  const { register, handleSubmit } = useForm<IBranch>();
  
  const createBranch = handleSubmit((values) => {
    api.post('/branchs', values).then((res) => {
      if (res.data.status) {
        setOpenAlert(true);
        setIsAdded(true);
        setNewBranchId(res.data.id);
      }
    });
  });

  return (
    <Common iconBtn namePage="Adicionar Unidade">
      <NewBranchArea>
        <FormArea onSubmit={createBranch}>
          <TextField
            id="standard-basic"
            label="Nome"
            variant="standard"
            required
            {...register('name')}
            // InputLabelProps={{style:}}
          />
          <TextField
            id="standard-basic"
            label="Endereço"
            variant="standard"
            required
            {...register('address')}
          />
          <TextField
            id="standard-basic"
            label="Cidade"
            variant="standard"
            required
            {...register('city')}
          />
          <TextField
            id="standard-basic"
            label="Estado"
            variant="standard"
            required
            {...register('state')}
          />
          <TextField
            id="standard-basic"
            label="Telefone"
            variant="standard"
            required
            {...register('phone')}
          />
          <TextField
            id="standard-basic"
            label="Email"
            variant="standard"
            required
            {...register('email')}
          />
          <ButtonArea variant="contained" color="success" type="submit">
            Salvar
          </ButtonArea>
        </FormArea>
      </NewBranchArea>
      <Dialog open={OpenAlert} onClose={handleClick}>
        <Alert color={isAdded ? 'success' : 'error'}>
          {isAdded ? 'Filial Adcionada' : 'Tentar Novamente'}
          <br />
          <br />
          {isAdded ? (
            // eslint-disable-next-line @next/next/link-passhref
            <Link href="/">
            <Button variant="text" color='info'>   <a>Home</a></Button>
            </Link>
          ): <Button variant="text" color='info' onClick={handleClick}>Tentar novamente</Button>}
          <br />
          <br />
          {isAdded && (
            // eslint-disable-next-line @next/next/link-passhref
            <Link href={`/branchs/${newBranchId}`}>
            <Button variant="text" color='info'> <a>Editar Filial</a></Button>
            </Link>
          )}
          <br />
          <br />
          {isAdded && <Button variant="text" color='info'>Adicionar outra filial</Button>}
          <Button variant="text" color="info" onClick={handleClick}>
            {' '}
            <Close />
          </Button>
        </Alert>
      </Dialog>
    </Common>
  );
}

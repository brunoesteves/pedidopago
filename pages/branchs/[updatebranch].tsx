import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import Common from '../../components/common';

import TextField from '@mui/material/TextField';
import Close from '@mui/icons-material/Close';
import { Alert, Button, Dialog } from '@mui/material';

import { ButtonArea, FormArea, NewBranchArea } from '../../styles/newBranch';

import { IBranch } from '../../types/branch';

import api from '../../components/api';

import Link from 'next/link';
import { useRouter } from 'next/router';
import { GetServerSideProps, NextPage } from 'next';

interface IBranchProps {
  updatebranchInfo: IBranch;
}

const UpdateBranch: NextPage<IBranchProps> = ({ updatebranchInfo }) => {
  const router = useRouter();
  const { updatebranch } = router.query;

  const [OpenAlert, setOpenAlert] = useState<boolean>(false);
  const [isUpdated, setIsUpdated] = useState<boolean>(true);
  
  const handleClick = () => {
    setOpenAlert(!open);
  };

  const { register, handleSubmit } = useForm<IBranch>({
    defaultValues: {
      name: updatebranchInfo.name,
      address: updatebranchInfo.address,
      email: updatebranchInfo.email,
      city: updatebranchInfo.city,
      state: updatebranchInfo.state,
      phone: updatebranchInfo.phone
    }
  });
  const updateBranchHandler = handleSubmit((values) => {
    console.log(updatebranch);
    api.post('/branchs', values).then((res) => {
      if (res.data.status) {
        api.post(`/branchs/${updatebranch}`, values).then((res) => {
          if (res.data) {
            setOpenAlert(true);
            setIsUpdated(true);
          }
        });
      }
    });
  });

  return (
    <Common iconBtn namePage="Editar Unidade">
      <NewBranchArea>
        <FormArea onSubmit={updateBranchHandler}>
          <TextField
            id="standard-basic"
            label="Nome"
            variant="standard"
            required
            {...register('name')}
          />
          <TextField
            id="standard-basic"
            label="Endereço"
            variant="standard"
            required
            {...register('address')}
          />
          <TextField
            id="standard-basic"
            label="Cidade"
            variant="standard"
            required
            {...register('city')}
          />
          <TextField
            id="standard-basic"
            label="Estado"
            variant="standard"
            required
            {...register('state')}
          />
          <TextField
            id="standard-basic"
            label="Telefone"
            variant="standard"
            required
            {...register('phone')}
          />
          <TextField
            id="standard-basic"
            label="Email"
            variant="standard"
            required
            {...register('email')}
          />
          <ButtonArea variant="contained" color="success" type="submit">
            Salvar
          </ButtonArea>
        </FormArea>
      </NewBranchArea>
      <Dialog open={OpenAlert} onClose={handleClick}>
        <Alert color={isUpdated ? 'success' : 'error'}>
          {isUpdated ? 'Filial Atualizada' : 'Tentar Novamente'}
          <br />
          <br />
          {isUpdated ? (
            // eslint-disable-next-line @next/next/link-passhref
            <Link href="/">
              <Button variant="text" color="info">
                {' '}
                <a>Home</a>
              </Button>
            </Link>
          ) : (
            <Button variant="text" color="info" onClick={handleClick}>
              Tentar novamente
            </Button>
          )}
          <Button variant="text" color="info" onClick={handleClick}>
            {' '}
            <Close />
          </Button>
        </Alert>
      </Dialog>
    </Common>
  );
};

export default UpdateBranch;

export const getServerSideProps: GetServerSideProps = async (context) => {
  const updatebranchID = context.params?.updatebranch;

  const { data } = await api.get(`/branchs/${updatebranchID}`);

  return {
    props: { updatebranchInfo: data }
  };
};

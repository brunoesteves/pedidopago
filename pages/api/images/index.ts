import type { NextApiRequest, NextApiResponse } from "next";
import formidable from "formidable";
import fs from "fs";

export const config = {
  api: {
    bodyParser: false,
  },
};

// eslint-disable-next-line import/no-anonymous-default-export
export default (req: NextApiRequest, res: NextApiResponse) => {
  const form = new formidable.IncomingForm();
  form.parse(req, function (_err, _fields, files) {
    saveFile(files.file);
    return res.status(201).send(true);
  });
};

const saveFile = (file: formidable.File | formidable.File[]) => {
  const picture = Array.isArray(file) ? file[0] : file;

  const data = fs.readFileSync(picture.filepath);
  fs.writeFileSync(`./public/${picture.originalFilename}`, data);
};

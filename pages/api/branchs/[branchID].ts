// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import { deletebranch, updateBranch, getUniquebranch } from '../../../repositories/branch';

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  const { branchID } = req.query;

  if (req.method === 'DELETE') {
    const getDeletebranch = await deletebranch(Number(branchID));
    if (getDeletebranch) {
      res.status(200).send(true);
    }
  } else if (req.method === 'GET') {
    const getBranchInfo = await getUniquebranch(Number(branchID));

    if (getBranchInfo) {
      res.status(200).send(getBranchInfo);
    }
  }else if (req.method === 'POST') {
    
    const updateBranchInfo = await updateBranch(Number(branchID), req.body);

    if (updateBranchInfo) {
      res.status(200).send(true);
    }
  }
}

// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import { branchList, createBranch } from '../../../repositories/branch';

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === 'GET') {
    const getBranchList = await branchList();
    res.status(200).json(getBranchList);
  } else if (req.method === 'POST') {
    const newBranch = await createBranch(req.body);
    console.log('api: ', newBranch);
    res.status(200).json({status: true, id: newBranch.id});
  }
}

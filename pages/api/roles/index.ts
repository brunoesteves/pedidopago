// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import { roleList, addRole } from '../../../repositories/role';

export default async function handler(req: NextApiRequest, res: NextApiResponse) {

  if (req.method === 'GET') {
    const getRolesList = await roleList();
    res.status(200).json(getRolesList);
  } else if (req.method === 'POST') {
    const { newRole } = req.body;

    const isAdd = await addRole(newRole);
    if (isAdd) {
      res.status(200).send(true);
    }
  }
}

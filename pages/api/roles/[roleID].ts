// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import { deleteRole } from '../../../repositories/role';

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  const { roleID } = req.query;

  if (req.method === 'DELETE') {
    const deleteRoleID = await deleteRole(Number(roleID));
    if (deleteRoleID) {
      res.status(200).send(true);
    }
  }
}

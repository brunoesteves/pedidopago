// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { employeeList, newEmployee } from "../../../repositories/employee";
import fs from "fs";

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === "GET") {
    const employees = await employeeList();
    res.status(200).json(employees);
  } else if (req.method === "POST") {

    const employees = await newEmployee(req.body);
    if (employees) {
      fs.renameSync("./public/0.jpg", `./public/${employees.id}.jpg`);
    }
    res.status(200).json({message: true, id: employees.id});
  }
}

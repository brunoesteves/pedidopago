// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import { employeeInfo, deleteEmployee, updateEmployee } from '../../../repositories/employee';

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  const { employeeID } = req.query;

  if (req.method === 'GET') {
    const getEmployeeInfo = await employeeInfo(Number(employeeID));
    res.status(200).json(getEmployeeInfo);
  } else if (req.method === 'DELETE') {
    const getEmployeeDelete = await deleteEmployee(Number(employeeID));
    if (getEmployeeDelete) {
      res.status(200).json(getEmployeeDelete);
    }
  } else if (req.method === 'POST') {
    const updateEmployeeInfo = await updateEmployee(Number(employeeID), req.body);
    if (updateEmployeeInfo) {
      res.status(200).send(true);
    }
  }
}

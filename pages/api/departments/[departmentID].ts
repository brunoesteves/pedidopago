// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import { deleteDepartment } from '../../../repositories/department';

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  const { roleID } = req.query;

  if (req.method === 'DELETE') {
    const deleteDepartmentID = await deleteDepartment(Number(roleID));
    if (deleteDepartmentID) {
      res.status(200).send(true);
    }
  }
}

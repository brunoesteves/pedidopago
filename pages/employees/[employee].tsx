import React, { useContext, useEffect, useState } from "react";

import ArticleIcon from "@mui/icons-material/Article";
import PhoneInTalkIcon from "@mui/icons-material/PhoneInTalk";
import Edit from "@mui/icons-material/Edit";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DatePicker from "@mui/lab/DatePicker";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import Close from "@mui/icons-material/Close";
import TextField from "@mui/material/TextField";

import { useRouter } from "next/router";
import { GetServerSideProps, NextPage } from "next";
import Link from "next/link";

import api from "../../components/api";

import { useForm } from "react-hook-form";
import Common from "../../components/common";
import UploadImages from "../../components/Employees/uploadImages";

import {
  EmployeeArea,
  EmployeeTop,
  Name_Email,
  SelectdArea,
  ImageEmployee,
  PersonalInfo,
  Information,
  TitlePersonalInfo,
  TextFieldSelect,
  TitleSelect,
  FormArea,
} from "../../styles/employee";

import { IEmployee } from "../../types/employee";
import { IBranch } from "../../types/branch";
import { IDepartment } from "../../types/departments";
import { IRole } from "../../types/roles";
import { FormControl, InputLabel, Stack, Button, IconButton, Dialog, Alert } from "@mui/material";

interface EmployeeProps {
  employeeInfo: IEmployee;
  branchslist: IBranch[];
  departmentslist: IDepartment[];
  roleslist: IRole[];
}
import { EmployeeContext } from "../../context/employee";

const Employee: NextPage<EmployeeProps> = ({
  employeeInfo,
  branchslist,
  departmentslist,
  roleslist,
}) => {
  const router = useRouter();
  const { employee } = router.query;

  const [openModal, setOpenModal] = useState<boolean>(false);
  const [OpenAlert, setOpenAlert] = useState<boolean>(false);
  const [isUpdated, setIsUpdated] = useState<boolean>(false);
  const [pic, setPic] = useState(`${employee}.jpg`);

  const { setName, setRole } = useContext(EmployeeContext);

  useEffect(() => {
    setName(employeeInfo.name);
    setRole(employeeInfo.email);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleClick = () => {
    setOpenAlert(!open);
  };

  function toOpenModal() {
    setOpenModal(true);
    setTimeout(() => {
      setOpenModal(false);
    }, 1000);
  }


 const { register, handleSubmit } = useForm<IEmployee>({
    defaultValues: {
      name: employeeInfo.name,
      email: employeeInfo.email,
      cpf: employeeInfo.cpf,
      phone: employeeInfo.phone,
      birth_date: employeeInfo.birth_date,
      departmentID: employeeInfo.departmentID,
      roleID: employeeInfo.roleID,
      branchID: employeeInfo.branchID,
      status: "active",
    },
  });
  const updateEmployeeHandler = handleSubmit((values) => {
    api.post(`/employees/${employee}`, values).then((res) => {
      if (res.data) {
        setOpenAlert(true);
        setIsUpdated(true);
      }
    });
  });
  
  function reloadImage(res: boolean) {
    if (res) {
      setPic(`${employee}.jpg?cache=${new Date().valueOf()}`);
    }
  }


  return (
    <Common iconBtn namePage="Editar Colaborador(a)">
      <EmployeeArea>
        <FormArea onSubmit={updateEmployeeHandler}>
          <EmployeeTop>
            <ImageEmployee
              src={`/${pic}`}
              key={1}
              width={80}
              height={80}
              objectFit="cover"
              loading="eager"
              priority={true}
            />
            <IconButton onClick={toOpenModal}>
              <Edit />
            </IconButton>
            <Name_Email>
              <Stack
                spacing={1}
                direction="row"
                sx={{ marginBottom: "10px", display: "flex", alignItems: "center" }}
              >
                <TextField
                  required
                  id="outlined-required"
                  label="Editar Nome"
                  {...register("name")}
                />
              </Stack>

              <Stack
                spacing={1}
                direction="row"
                sx={{ marginBottom: "10px", display: "flex", alignItems: "center" }}
              >
                <TextField
                  required
                  id="outlined-required"
                  label="Editar email"
                  {...register("email")}
                />
              </Stack>
            </Name_Email>{" "}
          </EmployeeTop>
          <TitlePersonalInfo>Informações Pessoais</TitlePersonalInfo>
          <PersonalInfo>
            <Information>
              <ArticleIcon />
              <div>
                <TextField
                  id="standard-helperText"
                  label="CPF"
                  {...register("cpf")}
                  variant="standard"
                  disabled={false}
                />
              </div>
            </Information>
            <Information>
              <PhoneInTalkIcon />
              <div>
                <TextField
                  id="standard-helperText"
                  label="Telefone"
                  {...register("phone")}
                  variant="standard"
                  disabled={false}
                />
              </div>
            </Information>
            <Information>
              <div>
                <Stack spacing={2} direction="row">
                  <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <DatePicker
                      disableFuture
                      openTo="day"
                      views={["year", "month", "day"]}
                      value={employeeInfo.birth_date.replace("T03:00:00.000Z", "")}
                      renderInput={(params) => <TextField {...params} />}
                      {...register("birth_date")}
                      onChange={() => {
                        console.log("");
                      }}
                    />
                  </LocalizationProvider>
                </Stack>
              </div>
            </Information>
          </PersonalInfo>
          <TextFieldSelect>
            <TitleSelect>Dados organizacionais</TitleSelect>
            <div>
              <FormControl sx={{ width: "50%", marginBottom: "15px" }}>
                <InputLabel id="demo-simple-select-label">Departamento</InputLabel>
                <SelectdArea {...register("departmentID")}>
                  {departmentslist.map((item) => {
                    return (
                      <option value={item.id} key={item.id} style={{ textAlign: "center" }}>
                        {item.name}
                      </option>
                    );
                  })}
                </SelectdArea>
              </FormControl>
              <FormControl sx={{ width: "50%", marginBottom: "15px" }}>
                <InputLabel id="demo-simple-select-label">Cargo</InputLabel>
                <SelectdArea {...register("roleID")}>
                  {roleslist.map((item) => {
                    return (
                      <option value={item.id} key={item.id} style={{ textAlign: "center" }}>
                        {item.name}
                      </option>
                    );
                  })}
                </SelectdArea>
              </FormControl>
              <FormControl sx={{ width: "50%", marginBottom: "15px" }}>
                <InputLabel id="demo-simple-select-label">Unidade</InputLabel>
                <SelectdArea {...register("branchID")}>
                  {branchslist.map((item) => {
                    return (
                      <option key={item.id} value={item.id} style={{ textAlign: "center" }}>
                        {item.name}
                      </option>
                    );
                  })}
                </SelectdArea>
              </FormControl>
              <FormControl sx={{ width: "50%", marginBottom: "15px" }}>
                <InputLabel id="demo-simple-select-label">Status</InputLabel>
                <SelectdArea {...register("status")}>
                  <option value="active" style={{ textAlign: "center" }}>
                    Active
                  </option>
                  <option value="inactive" style={{ textAlign: "center" }}>
                    Inactive
                  </option>
                </SelectdArea>
              </FormControl>
            </div>
          </TextFieldSelect>
          <Stack spacing={2} direction="row" sx={{ marginTop: "10px" }}>
            <Button variant="contained" color="success" type="submit">
              Salvar
            </Button>
          </Stack>
        </FormArea>
      </EmployeeArea>
      <UploadImages openModal={openModal} reloadImage={reloadImage} employee={router.query.employee?.toString() || "0"} />
      <Dialog open={OpenAlert} onClose={handleClick}>
        <Alert color={isUpdated ? "success" : "error"}>
          {isUpdated ? "Colaborador Atualizado(s)" : "Tentar Novamente"}
          <br />
          <br />
          {isUpdated ? (
            // eslint-disable-next-line @next/next/link-passhref
            <Link href="/">
              <Button variant="text" color="info">
                {" "}
                <a>Home</a>
              </Button>
            </Link>
          ) : (
            <Button variant="text" color="info" onClick={handleClick}>
              Tentar novamente
            </Button>
          )}
          <Button variant="text" color="info" onClick={handleClick}>
            {" "}
            <Close />
          </Button>
        </Alert>
      </Dialog>
    </Common>
  );
};

export default Employee;

export const getServerSideProps: GetServerSideProps = async (context) => {
  const employeeID = context.params?.employee;

  const getEmployeeInfo = await api.get(`/employees/${employeeID}`);

  const getAllBranchs = await api.get("/branchs");
  const getAllDepartments = await api.get("/departments");
  const getallRoles = await api.get("/roles");

  const employeeInfo = getEmployeeInfo.data;
  const branchslist = getAllBranchs.data;
  const departmentslist = getAllDepartments.data;
  const roleslist = getallRoles.data;

  return {
    props: { employeeInfo, branchslist, departmentslist, roleslist },
  };
};

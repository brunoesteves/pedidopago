import React, { useState } from "react";

import ArticleIcon from "@mui/icons-material/Article";
import PhoneInTalkIcon from "@mui/icons-material/PhoneInTalk";
import Edit from "@mui/icons-material/Edit";
import Close from "@mui/icons-material/Close";
import TextField from "@mui/material/TextField";

import { useRouter } from "next/router";
import { GetServerSideProps, NextPage } from "next";

import api from "../../../components/api";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";

import EmployeeSchema from "../../../schemas/employes";

import Common from "../../../components/common";
import UploadImages from "../../../components/Employees/uploadImages";

import {
  EmployeeArea,
  EmployeeTop,
  Name_Email,
  SelectdArea,
  ImageEmployee,
  PersonalInfo,
  Information,
  TitlePersonalInfo,
  TextFieldSelect,
  TitleSelect,
  InputDate,
  FormArea,
} from "../../../styles/employee";

import { IEmployee } from "../../../types/employee";
import { IBranch } from "../../../types/branch";
import { IDepartment } from "../../../types/departments";
import { IRole } from "../../../types/roles";
import { FormControl, Stack, Button, IconButton, Dialog, Alert } from "@mui/material";

interface EmployeeProps {
  employeeInfo: IEmployee;
  branchslist: IBranch[];
  departmentslist: IDepartment[];
  roleslist: IRole[];
}

const Employee: NextPage<EmployeeProps> = ({ branchslist, departmentslist, roleslist }) => {
  const router = useRouter();

  const [openModal, setOpenModal] = useState<boolean>(false);
  const [OpenAlert, setOpenAlert] = useState<boolean>(false);
  const [isAdded, setIsAdded] = useState<boolean>(false);
  const [pic, setPic] = useState("avatar.jpg");
  const [employeeID, setEmployeeID] = useState<number>(0);

  const handleClick = () => {
    setOpenAlert(!open);
  };

  function toOpenModal() {
    setOpenModal(true);
    setTimeout(() => {
      setOpenModal(false);
    }, 1000);
  }

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IEmployee>({
    mode: "onBlur",
    resolver: yupResolver(EmployeeSchema),
  });

  const newEmployeeHandler = handleSubmit((values) => {
    console.log(values);
    if (values) {
      values = { ...values, image: pic };
      api.post(`/employees`, values).then((res) => {
        if (res.data.message) {
          setOpenAlert(true);
          setIsAdded(true);
          setEmployeeID(res.data.id);
        }
      });
    }
  });

  function reloadImage(res: boolean) {
    if (res) {
      setPic(`0.jpg?cache=${new Date().valueOf()}`);
    }
  }

  return (
    <Common iconBtn namePage="Adicionar Colaborador(a)">
      <EmployeeArea>
        <FormArea onSubmit={newEmployeeHandler}>
          <EmployeeTop>
            <ImageEmployee
              src={`/${pic}`}
              key={1}
              width={80}
              height={80}
              objectFit="cover"
              loading="eager"
              priority={true}
            />
            <IconButton onClick={toOpenModal}>
              <Edit />
            </IconButton>
            <Name_Email>
              <Stack
                spacing={1}
                direction="row"
                sx={{ marginBottom: "10px", display: "flex", alignItems: "center" }}
              >
                <TextField
                  id="outlined-required"
                  label="Editar Nome"
                  {...register("name")}
                  error={Boolean(errors?.name?.message)}
                />
                <span>{errors?.name?.message}</span>
              </Stack>

              <Stack
                spacing={1}
                direction="row"
                sx={{ marginBottom: "10px", display: "flex", alignItems: "center" }}
              >
                <TextField
                  id="outlined-required"
                  label="Editar email"
                  {...register("email")}
                  error={Boolean(errors?.email)}
                />
                <span>{errors?.email?.message}</span>
              </Stack>
              <Stack
                spacing={1}
                direction="row"
                sx={{ marginBottom: "10px", display: "flex", alignItems: "center" }}
              >
                <TextField
                  id="outlined-required"
                  label="Editar endereço"
                  {...register("address")}
                  error={Boolean(errors?.address)}
                />
                <span>{errors?.email?.message}</span>
              </Stack>
            </Name_Email>
          </EmployeeTop>
          <TitlePersonalInfo>Informações Pessoais</TitlePersonalInfo>
          <PersonalInfo>
            <Information>
              <ArticleIcon />
              <div>
                <TextField
                  error={Boolean(errors?.cpf?.message)}
                  label="CPF"
                  {...register("cpf")}
                  variant="standard"
                  disabled={false}
                />
                <span>{errors?.cpf?.message}</span>
              </div>
            </Information>
            <Information>
              <PhoneInTalkIcon />
              <div>
                <TextField
                  error={Boolean(errors?.phone?.message)}
                  id="standard-helperText"
                  label="Telefone"
                  {...register("phone")}
                  variant="standard"
                  disabled={false}
                />
                <span>{errors?.phone?.message}</span>
              </div>
            </Information>
            <Information>
              <div>
                <Stack spacing={2} direction="row">
                  <InputDate type="date" {...register("birth_date")} />
                </Stack>
              </div>
            </Information>
          </PersonalInfo>
          <TextFieldSelect>
            <TitleSelect>Dados organizacionais</TitleSelect>
            <div>
              <FormControl sx={{ width: "50%", marginBottom: "15px" }}>
                <SelectdArea
                  {...register("departmentID")}
                  style={{
                    borderColor:
                      Boolean(errors?.departmentID?.message) && register("departmentID")
                        ? "red"
                        : "black",
                  }}
                >
                  <option style={{ textAlign: "center" }}>Selecione um departamento</option>
                  {departmentslist.map((item) => {
                    return (
                      <>
                        <option value={item.id} key={item.id} style={{ textAlign: "center" }}>
                          {item.name}
                        </option>
                      </>
                    );
                  })}
                </SelectdArea>
              </FormControl>
              <FormControl sx={{ width: "50%", marginBottom: "15px" }}>
                <SelectdArea
                  {...register("roleID")}
                  style={{
                    borderColor:
                      Boolean(errors?.roleID?.message) && register("roleID") ? "red" : "black",
                  }}
                >
                  <option style={{ textAlign: "center" }}>Selecione um Cargo</option>
                  {roleslist.map((item) => {
                    return (
                      <option value={item.id} key={item.id} style={{ textAlign: "center" }}>
                        {item.name}
                      </option>
                    );
                  })}
                </SelectdArea>
              </FormControl>
              <FormControl sx={{ width: "50%", marginBottom: "15px" }}>
                <SelectdArea
                  {...register("branchID")}
                  style={{
                    borderColor:
                      Boolean(errors?.departmentID?.message) && register("branchID")
                        ? "red"
                        : "black",
                  }}
                >
                  <option style={{ textAlign: "center" }}>Selecione uma Unidade</option>
                  {branchslist.map((item) => {
                    return (
                      <option key={item.id} value={item.id} style={{ textAlign: "center" }}>
                        {item.name}
                      </option>
                    );
                  })}
                </SelectdArea>
              </FormControl>
              <FormControl sx={{ width: "50%", marginBottom: "15px" }}>
                <SelectdArea {...register("status")}>
                  <option value="active" style={{ textAlign: "center" }}>
                    Active
                  </option>
                  <option value="inactive" style={{ textAlign: "center" }}>
                    Inactive
                  </option>
                </SelectdArea>
              </FormControl>
            </div>
          </TextFieldSelect>
          <Stack spacing={2} direction="row" sx={{ marginTop: "10px" }}>
            <Button variant="contained" color="success" type="submit">
              Salvar
            </Button>
          </Stack>
        </FormArea>
      </EmployeeArea>
      <UploadImages
        openModal={openModal}
        reloadImage={reloadImage}
        employee={router.query.employee?.toString() || "0"}
      />
      <Dialog open={OpenAlert} onClose={handleClick}>
        <Alert color={isAdded ? "success" : "error"}>
          {isAdded ? (
            router.push(`/employees/${employeeID}`)
          ) : (
            <>
              "Tentar Novamente"
              <br />
              <br />
              <Button variant="text" color="info" onClick={handleClick}>
                Tentar novamente
              </Button>
              <Button variant="text" color="info" onClick={handleClick}>
                <Close />
              </Button>
            </>
          )}
        </Alert>
      </Dialog>
    </Common>
  );
};

export default Employee;

export const getServerSideProps: GetServerSideProps = async () => {
  const getAllBranchs = await api.get("/branchs");
  const getAllDepartments = await api.get("/departments");
  const getallRoles = await api.get("/roles");

  const branchslist = getAllBranchs.data;
  const departmentslist = getAllDepartments.data;
  const roleslist = getallRoles.data;

  return {
    props: { branchslist, departmentslist, roleslist },
  };
};

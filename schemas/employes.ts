import yup from "../utils/yup";

const EmployeeSchema = yup.object().shape({
  name: yup.string().required(),
  email: yup.string().required(),
  address: yup.string().required(),
  phone: yup.number().required(),
  cpf: yup.number().required(),
  roleID: yup.number().required(),
});

export default EmployeeSchema;

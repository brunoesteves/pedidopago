import { NextPage } from 'next';
import React, { useEffect, useState } from 'react';
import { IconButton, Menu, MenuItem } from '@mui/material';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import VisibilityIcon from '@mui/icons-material/Visibility';
import ArrowDropUpIcon from '@mui/icons-material/ArrowDropUp';

import { useRouter } from 'next/router';


import DataTable, { TableColumn } from 'react-data-table-component';
import customTableStyle from '../../utils/table';
import customTableStyleMobile from '../../utils/tableMobile';
import api from '../api';

import { IEmployee } from '../../types/employee';
type EmployeePageProps = {
  data: IEmployee[];
  filterText: string;
};

const DataTableEmployee: NextPage<EmployeePageProps> = (props) => {
  const router = useRouter();
  const [anchorEl, setAnchorEl] = useState<Element | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [employeePage, setEmployeePage] = useState<number>(1);
  const [firstItem, setFirstItem] = useState<number>(0);
  const [lastItem, setLastItem] = useState<number>(10);
  const [curPage, setCurPage] = useState<number>(1);
  const [perPage, setPerPage] = useState<number>(10);
  const [sizeScreen, setSizeScreen] = useState<number>(0);

  useEffect(() => {
    const { innerWidth: width } = window;
    if (props.data) {
      setLoading(false);
      setSizeScreen(width);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  //

  function handleClick(event: React.MouseEvent<HTMLButtonElement>) {
    const { currentTarget } = event;
    setAnchorEl(currentTarget);
  }

  const filteredItems = props.data.filter(
    (item) => item.name && item.name.toLowerCase().includes(props.filterText.toLowerCase())
  );

  const caseInsensitiveSort = (rowA: { status: 'active' | 'inactive'; }, rowB: { status: 'active' | 'inactive'; }) => {
    console.log(rowA.status);
    
    const a = rowA.status;
    const b = rowB.status;

    if (a > b) {
      return 1;
    }

    if (b > a) {
      return -1;
    }

    return 0;
  };

  const columns: TableColumn<IEmployee>[] = [
    {
      name: '',
      selector: (row) => row.name

      // selector: () => <ImageEmployee src="/face.jpg" width={25} height={25} objectFit="cover" />
    },
    {
      id:"name",
      name: 'Nome',
      selector: (row) => row.name
    },
    {
      name: 'Departamento',
      selector: (row) => `${row.department.name}`
    },
    {
      name: 'Cargo',

      cell: (row) => `${row.role.name}`
    },
    {
      name: 'filial',

      cell: (row) => `${row.branch.name}`
    },
    {
      name: 'Status',
      cell: (row) => (
        <span
          style={{
            backgroundColor: row.status === "active" ? '#b5f1dd' : '#EAEFED',
            borderRadius: 10,
            padding: 5,
            paddingLeft: 10,
            paddingRight: 10
          }}>
          {row.status === "active"? 'active' : 'inactive'}
        </span>
      ),
      sortable: true,
      sortFunction: caseInsensitiveSort
    },
    {
      name: '',
      // selector: (row) => row.storeID,
      cell: (row) => (
        <IconButton onClick={handleClick}>
          <MoreVertIcon onClick={() => setEmployeePage(row.id)} />
        </IconButton>
      )
    }
  ];

  function handlePageChange(newPage: number) {
    if (newPage > curPage) {
      setFirstItem((firstItem) => firstItem + perPage);
      setLastItem((lastItem) => lastItem + perPage);
      setCurPage(newPage);
    } else {
      setFirstItem((firstItem) => firstItem - perPage);
      setLastItem((lastItem) => lastItem - perPage);
      setCurPage(newPage);
    }
  }

  const paginationComponentOptions = {
    rowsPerPageText: 'Colaboradores por página',
    rangeSeparatorText: 'de',
    selectAllRowsItem: false
  };

  useEffect(() => {
    function handleResize() {
      setSizeScreen(window.innerWidth);
    }
    window.addEventListener('resize', handleResize);
  });

  function deleteEmployee(){
    api.delete(`/employees/${employeePage}`)
    .then(res =>{
      if(res.data){
        setAnchorEl(null)
        router.replace(router.asPath);

      }
    })
  }

  return (
    <>
      <DataTable
        columns={columns}
        data={filteredItems.slice(firstItem, lastItem)}
        customStyles={sizeScreen < 820 ? customTableStyleMobile : customTableStyle}
        progressPending={loading}
        pagination
        paginationServer
        paginationTotalRows={filteredItems.length}
        onChangeRowsPerPage={(newPerPage: number) => {
          setPerPage(newPerPage);
          setCurPage(1);
          setFirstItem(0);
          setLastItem(newPerPage);
        }}
        onChangePage={(newPage) => handlePageChange(newPage)}
        paginationDefaultPage={curPage}
        fixedHeader
        paginationComponentOptions={paginationComponentOptions}
        sortIcon={<ArrowDropUpIcon />}
        defaultSortFieldId="name"
        defaultSortAsc={true}
      />
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button'
        }}>
        <MenuItem onClick={() => router.push(`/employees/${employeePage}`)}>
          <VisibilityIcon sx={{ marginRight: '10px', color: 'rgba(79, 216, 163, 0.4)' }} />
          Ver Colaborador{' '}
        </MenuItem>
        <MenuItem onClick={deleteEmployee}>
          <DeleteOutlineIcon sx={{ marginRight: '10px', color: 'rgba(163,184,176,0.4)' }} />
          Excluir{' '}
        </MenuItem>
      </Menu>
    </>
  );
};

export default DataTableEmployee;

import React, { useState } from 'react';
import TextField from '@mui/material/TextField';
import SearchIcon from '@mui/icons-material/Search';

import { EmployeesArea, EmployeesTable, Title } from '../../styles/employees';

import { NextPage } from 'next';
import { useRouter } from 'next/router';


import DataTableEmployee from './dataTableEmployees';

import { IEmployee } from '../../types/employee';
import { Button } from '@mui/material';

type EmployeePageProps = {
  employeesList: IEmployee[];
};

const EmployeePage: NextPage<EmployeePageProps> = ({ employeesList }) => {
  const router = useRouter();

  const [filterText, setFilterText] = useState<string>('');
  return (
    <>
      <EmployeesArea>
        <TextField
          id="outlined-helperText"
          label="Pesquisar por"
          placeholder="Pesquise por nome"
          InputProps={{
            startAdornment: <SearchIcon />
          }}
          fullWidth
          onChange={(e) => setFilterText(e.target.value)}
        />
        <EmployeesTable>
        <Title>Listagem das Colaboradores</Title>
          <Button
            variant="contained"
            size="large"
            sx={{ width: '230px', height: '50px' }}
            color="primary"
            onClick={() => router.push('/employees/newemployee')}>
            Adicionar Colaborador
          </Button>
        </EmployeesTable>
      </EmployeesArea>
      <DataTableEmployee data={employeesList} filterText={filterText} />
    </>
  );
};

export default EmployeePage;

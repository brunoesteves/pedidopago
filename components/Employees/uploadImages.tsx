import React, { useCallback, useEffect, useState } from 'react';
import { NextPage } from 'next';

import { Modal, Box, Button } from '@mui/material';
import LinearProgress, { linearProgressClasses } from '@mui/material/LinearProgress';
import { styled } from '@mui/material/styles';

import api from '../api';
import { useDropzone } from 'react-dropzone';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
  cursor: "pointer"
};

type UploadImagesProps = {
  openModal: boolean;
  employee: string;
  reloadImage: any;
};

const UploadImages: NextPage<UploadImagesProps> = (props) => {
  const [open, setOpen] = useState<boolean>(false);
  const [file, setFile] = useState<string>('');
  const [uploadPercentage, setUploadPercentage] = useState<number>(0);

  const handleClose = () => setOpen(false);
  useEffect(() => {
    if (props.openModal) {
      setOpen(true);
    }
  }, [props.openModal]);

  const onDrop = useCallback((acceptedFiles) => {
    setFile(acceptedFiles[0]);
  }, []);

  // eslint-disable-next-line no-unused-vars
  function uploadFile(this: HTMLFormElement) {
    const nameFile = `${props.employee}.jpg`;
    const body = new FormData(this);
    if (body) {
      body.append('file', file, nameFile);
      api
        .post('/images', body, {
          onUploadProgress: (progressEvent) => {
            setUploadPercentage(
              Number(Math.round((progressEvent.loaded / progressEvent.total) * 100))
            );
          }
        })
        .then((res) => {
          if(res.data ===true){
            setFile("")
            handleClose()
            props.reloadImage(true)
          }
        });
    }
  }

  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });

  const BorderLinearProgress = styled(LinearProgress)(({ theme }) => ({
    height: 30,
    borderRadius: 5,
    marginTop: 10,
    [`&.${linearProgressClasses.colorPrimary}`]: {
      backgroundColor: theme.palette.grey[theme.palette.mode === 'light' ? 200 : 800]
    },
    [`& .${linearProgressClasses.bar}`]: {
      borderRadius: 5,
      backgroundColor: theme.palette.mode === 'light' ? '#1a90ff' : '#308fe8'
    }
  }));
  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description">
      <Box sx={style}>
        <div {...getRootProps()}>
          <input {...getInputProps()} />
          {isDragActive ? <p>Drop the files here ...</p> : <p>Arraste ou selecione os arquivos</p>}
        </div>
        <label htmlFor="contained-button-file"></label>
        <Button variant="contained" component="span" onClick={uploadFile}>
          Enviar
        </Button>
        {file && <BorderLinearProgress variant="determinate" value={uploadPercentage} /> }
      </Box>
    </Modal>
  );
};

export default UploadImages;

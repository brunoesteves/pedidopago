import React, { useState } from 'react';
import TextField from '@mui/material/TextField';
import SearchIcon from '@mui/icons-material/Search';
import DataTableRoles from './dataTableRoles';
import { useRouter } from 'next/router';

import { PositionsArea, PositionsTable, Title } from '../../styles/commonTabs';
import { NextPage } from 'next';

import { IRole } from '../../types/roles';
import { Button } from '@mui/material';
import api from '../api';

type RoleProps = {
  Roleslist: IRole[];
};

const RolesPage: NextPage<RoleProps> = ({ Roleslist }) => {
  const router = useRouter();
  const [filterText, setFilterText] = useState<string>('');
  const [newRole, setNewRole] = useState<string>('');
  const [saveButtonName, setSaveButtonName] = useState<string>('Salvar');
  const [isAdded, setIsAdded] = useState<boolean>(false);

  function saveRole() {
    setSaveButtonName('Salvando...');
    api.post(`/roles`, { newRole }).then((res) => {
      if (res.data === true) {
        setSaveButtonName('Salvo');
        setIsAdded(true);
        router.replace(router.asPath);
        setTimeout(() => {
          setSaveButtonName('Salvar');
          setNewRole('');
        }, 3000);
      } else {
        setSaveButtonName('Erro');
      }
    });
  }

  return (
    <>
      <PositionsArea>
        <TextField
          id="outlined-helperText"
          label="Pesquisar por"
          placeholder="Pesquise por Cargos"
          InputProps={{
            startAdornment: <SearchIcon />
          }}
          fullWidth
          onChange={(e) => setFilterText(e.target.value)}
        />
        <PositionsTable>
            <Title>Listagem de Cargos</Title>
            <TextField
              id="standard-basic"
              label="Adicionar Novo Cargo"
              variant="standard"
              value={newRole}
              onChange={(e) => setNewRole(e.target.value)}
            />
            <Button
              variant="contained"
              size="large"
              sx={{ width: '200px' }}
              color={
                saveButtonName === 'Salvar' ? 'primary' : isAdded == false ? 'secondary' : 'success'
              }
              onClick={saveRole}>
              {saveButtonName}
            </Button>
        </PositionsTable>
      </PositionsArea>
      <DataTableRoles data={Roleslist} filterText={filterText} />
    </>
  );
};

export default RolesPage;

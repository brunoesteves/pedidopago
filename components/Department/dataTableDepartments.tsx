import { NextPage } from 'next';
import React, { useEffect, useState } from 'react';
import { IconButton} from '@mui/material';
import Clear from '@mui/icons-material/Clear';
import ArrowDropUpIcon from '@mui/icons-material/ArrowDropUp';

import DataTable, { TableColumn } from 'react-data-table-component';
import customTableStyle from '../../utils/table';

import { IDepartment } from '../../types/departments';
type DepartmentsPageProps = {
  data: IDepartment[];
  filterText: string;
};

const DataTableBranchs: NextPage<DepartmentsPageProps> = (props) => {
  const [loading, setLoading] = useState<boolean>(true);
  const [firstItem, setFirstItem] = useState<number>(0);
  const [lastItem, setLastItem] = useState<number>(10);
  const [curPage, setCurPage] = useState<number>(1);
  const [perPage, setPerPage] = useState<number>(10);

  useEffect(() => {
    if (props.data) {
      setLoading(false);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  function deleteDepartment(id: number){
    alert(id);
    
  }

  const filteredItems = props.data.filter(
    (item) => item.name && item.name.toLowerCase().includes(props.filterText.toLowerCase())
  );

  const caseInsensitiveSort = (rowA: { name: string }, rowB: { name: string }) => {
    const a = rowA.name;
    const b = rowB.name;

    if (a > b) {
      return 1;
    }

    if (b > a) {
      return -1;
    }

    return 0;
  };

  const columns: TableColumn<IDepartment>[] = [
    {
      name: 'Nome',
      selector: (row) => row.name,
      sortable: true,
      sortFunction: caseInsensitiveSort
    },    
    {
      name: '',
      cell: (row) => (
        <IconButton >
          <Clear onClick={() => deleteDepartment(row.id)} />
        </IconButton>
      )
    }
  ];

  function handlePageChange(newPage: number) {
    if (newPage > curPage) {
      setFirstItem((firstItem) => firstItem + perPage);
      setLastItem((lastItem) => lastItem + perPage);
      setCurPage(newPage);
    } else {
      setFirstItem((firstItem) => firstItem - perPage);
      setLastItem((lastItem) => lastItem - perPage);
      setCurPage(newPage);
    }
  }

  const paginationComponentOptions = {
    rowsPerPageText: 'Departamentos por página',
    rangeSeparatorText: 'de',
    selectAllRowsItem: false
  };

  return (
    <>
      <DataTable
        columns={columns}
        data={filteredItems.slice(firstItem, lastItem)}
        customStyles={customTableStyle}
        progressPending={loading}
        pagination
        paginationServer
        paginationTotalRows={filteredItems.length}
        onChangeRowsPerPage={(newPerPage: number) => {
          setPerPage(newPerPage);
          setCurPage(1);
          setFirstItem(0);
          setLastItem(newPerPage);
        }}
        onChangePage={(newPage) => handlePageChange(newPage)}
        paginationDefaultPage={curPage}
        fixedHeader
        paginationComponentOptions={paginationComponentOptions}
        sortIcon={<ArrowDropUpIcon />}
      />
       
    </>
  );
};

export default DataTableBranchs;

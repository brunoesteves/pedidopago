import { NextPage } from 'next';
import React, { useEffect, useState } from 'react';
import { IconButton, Menu, MenuItem } from '@mui/material';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import VisibilityIcon from '@mui/icons-material/Visibility';
import ArrowDropUpIcon from '@mui/icons-material/ArrowDropUp';

import { useRouter } from 'next/router';

import DataTable, { TableColumn } from 'react-data-table-component';
import customTableStyle from '../../utils/table';

import { IBranch } from '../../types/branch';
import api from '../api';
type BranchsPageProps = {
  data: IBranch[];
  filterText: string;
};

const DataTableBranchs: NextPage<BranchsPageProps> = (props) => {
  const router = useRouter();
  const [anchorEl, setAnchorEl] = useState<Element | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [branchPage, setBranchPage] = useState<number>(0);
  const [firstItem, setFirstItem] = useState<number>(0);
  const [lastItem, setLastItem] = useState<number>(10);
  const [curPage, setCurPage] = useState<number>(1);
  const [perPage, setPerPage] = useState<number>(10);

  useEffect(() => {
    if (props.data) {
      setLoading(false);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  function handleClick(event: React.MouseEvent<HTMLButtonElement>) {
    const { currentTarget } = event;
    setAnchorEl(currentTarget);
  }

  const filteredItems = props.data.filter(
    (item) => item.name && item.name.toLowerCase().includes(props.filterText.toLowerCase())
  );

  const caseInsensitiveSort = (rowA: { name: string }, rowB: { name: string }) => {
    const a = rowA.name;
    const b = rowB.name;

    if (a > b) {
      return 1;
    }

    if (b > a) {
      return -1;
    }

    return 0;
  };

  const columns: TableColumn<IBranch>[] = [
    {
      id: 'name',
      name: 'Nome',
      selector: (row) => row.name,
      sortable: true,
      sortFunction: caseInsensitiveSort
    },
    {
      name: 'Endereço',

      selector: (row) => row.address
    },
    {
      name: 'Cidade',

      cell: (row) => row.city
    },
    {
      name: 'Estado',

      cell: (row) => row.state
    },
    {
      name: 'Telefone',

      cell: (row) => row.phone
    },
    {
      name: '',
      cell: (row) => (
        <IconButton onClick={handleClick}>
          <MoreVertIcon onClick={() => setBranchPage(row.id)} />
        </IconButton>
      )
    }
  ];

  function handlePageChange(newPage: number) {
    if (newPage > curPage) {
      setFirstItem((firstItem) => firstItem + perPage);
      setLastItem((lastItem) => lastItem + perPage);
      setCurPage(newPage);
    } else {
      setFirstItem((firstItem) => firstItem - perPage);
      setLastItem((lastItem) => lastItem - perPage);
      setCurPage(newPage);
    }
  }

  const paginationComponentOptions = {
    rowsPerPageText: 'Colaboradores por página',
    rangeSeparatorText: 'de',
    selectAllRowsItem: false
  };

  function deleteBranch(){
    api.delete(`/branchs/${branchPage}`)
    .then(res =>{
      if(res.data){
        setAnchorEl(null)
        router.replace(router.asPath);

      }
    })
  }

  return (
    <>
      <DataTable
        columns={columns}
        data={filteredItems.slice(firstItem, lastItem)}
        customStyles={customTableStyle}
        progressPending={loading}
        pagination
        paginationServer
        paginationTotalRows={filteredItems.length}
        onChangeRowsPerPage={(newPerPage: number) => {
          setPerPage(newPerPage);
          setCurPage(1);
          setFirstItem(0);
          setLastItem(newPerPage);
        }}
        onChangePage={(newPage) => handlePageChange(newPage)}
        paginationDefaultPage={curPage}
        fixedHeader
        paginationComponentOptions={paginationComponentOptions}
        sortIcon={<ArrowDropUpIcon />}
        defaultSortFieldId="name"
        defaultSortAsc={true}
      />
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button'
        }}>
        <MenuItem onClick={() => router.push(`/branchs/${branchPage}`)}>
          <VisibilityIcon sx={{ marginRight: '10px', color: 'rgba(79, 216, 163, 0.4)' }} />
          Ver Unidade{' '}
        </MenuItem>
        <MenuItem onClick={deleteBranch}>
          <DeleteOutlineIcon sx={{ marginRight: '10px', color: 'rgba(163,184,176,0.4)' }} />
          Excluir{' '}
        </MenuItem>
      </Menu>
    </>
  );
};

export default DataTableBranchs;

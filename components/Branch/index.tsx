import React, { useState } from 'react';
import TextField from '@mui/material/TextField';
import SearchIcon from '@mui/icons-material/Search';
import DataTableBranchs from './dataTableBranchs';

import { PositionsArea, PositionsTable, Title } from '../../styles/commonTabs';

import { NextPage } from 'next';
import { useRouter } from 'next/router';

import { IBranch } from '../../types/branch';
import { Button } from '@mui/material';

type BranchProps = {
  Branchslist: IBranch[];
};

const BranchsPage: NextPage<BranchProps> = ({ Branchslist }) => {
  const router = useRouter();

  const [filterText, setFilterText] = useState<string>('');

  return (
    <>
      <PositionsArea>
        <TextField
          id="outlined-helperText"
          label="Pesquisar por"
          placeholder="Pesquise por Filiais"
          InputProps={{
            startAdornment: <SearchIcon />
          }}
          fullWidth
          onChange={(e) => setFilterText(e.target.value)}
        />
        <PositionsTable>
          <Title>Listagem das Filias</Title>
          <Button
            variant="contained"
            size="large"
            sx={{ width: '230px', height: '50px' }}
            color="primary"
            onClick={() => router.push('/branchs/newbranch')}>
            Adicionar Filial
          </Button>
        </PositionsTable>
      </PositionsArea>
      <DataTableBranchs data={Branchslist} filterText={filterText} />
    </>
  );
};
export default BranchsPage;

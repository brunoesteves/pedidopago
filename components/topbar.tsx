import Image from "next/image";
import React, { useContext } from "react";

import {EmployeeContext} from "../context/employee"

import {
  TopBarDesktop,
  TopBarMobile,
  TopBarItemLeft,
  TopBarItemRight,
  MyData,
  NameUser,
} from "../styles/topbar";

const Topbar: React.FC = () => {

const {name, role} = useContext(EmployeeContext)
  return (
    <>
      <TopBarDesktop>
        <TopBarItemLeft>
          <Image src="/logotype.png" alt="Logo" width={50} height={50} />
        </TopBarItemLeft>
        <TopBarItemRight>
          <MyData>{name.charAt(0)}</MyData>
          <NameUser>
            <div>{name}</div>
            <div>{role}</div>
          </NameUser>
        </TopBarItemRight>
      </TopBarDesktop>
      <TopBarMobile>
        <TopBarItemLeft>
          <MyData>LZ</MyData>
        </TopBarItemLeft>
        <TopBarItemRight>
        <Image src="/logotype.png" alt="Logo" width={50} height={50} />
        </TopBarItemRight>

        <div></div>
      </TopBarMobile>
    </>
  );
};

export default Topbar;

import type { NextPage } from 'next';
import Head from 'next/head';
import Image from 'next/image';
import { useRouter } from 'next/router';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';

import {
  MainContent,
  TableArea,
  Container,
  FooterArea,
  Title,
  SideBar,
  HomeBody
} from '../styles/common';

import Topbar from './topbar';
import { IconButton } from '@mui/material';

type TitlePage = {
  iconBtn: boolean;
  namePage: string;
  // children: {};
};

const Common: NextPage<TitlePage> = (props) => {
  const router = useRouter();

  function handleClick() {
    router.push('/');
  }

  return (
    <Container>
      <Head>
        <title>Pedido Pago Web Challenge</title>
        <meta name="description" content="Pedido Pago Web Challenge" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Topbar />
      <HomeBody>
        <SideBar />
        <MainContent>
          <TableArea>
            <Title>
              {!props.iconBtn ? (
                ''
              ) : (
                <IconButton onClick={() => handleClick()}>
                  <ArrowBackIcon sx={{ backgroundColor: '#EAEFED', borderRadius: '50px' }} />
                </IconButton>
              )}
              {props.namePage}
            </Title>
            <Box
              sx={{
                display: 'flex',
                flexWrap: 'wrap',
                '& > :not(style)': {
                  m: 1,
                  width: '70vw',
                  paddingBottom:"0px" 
                }
              }}
              // height={{
              //   base: 887,
              //   md: 884,
              //   xl: 884
              // }}
              >
              <Paper elevation={3}>{props.children}</Paper>
            </Box>
          </TableArea>

          <FooterArea>
            <Image src="/logotype.png" alt="PedidoPago Logo" width={72} height={72} />
          </FooterArea>
        </MainContent>
      </HomeBody>
    </Container>
  );
};

export default Common;

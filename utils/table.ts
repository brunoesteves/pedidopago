import { minWidth } from '@mui/system';

const customTableStyle = {
  table: {
    style: {
      minHeight: '50vh',
      maxHeight: '68vh',
    }
  }, 
  cells:{
    style:{
      textTransform: "capitalize",
    }
  }
};

export default customTableStyle;

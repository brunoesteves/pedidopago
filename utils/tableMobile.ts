import { minWidth } from '@mui/system';

const customTableStyleMobile = {
  table: {
    style: {
      minHeight: '50vh',
      maxHeight: '68vh',
      width:"65vw"
    }
  },
  rows: {
    style: {
      flexDirection: 'column',
      marginBottom: "20px"
    }
  },
  cells: {
    style: {
      width: '100vw',
      display: 'flex',
      justifyContent: 'space-around',
      alignItems: 'flex-start',
      marginBottom: '10px',
      textTransform: "capitalize",

    }
  },
  head: { style: { display: 'none' } }
};

export default customTableStyleMobile;

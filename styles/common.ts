import styled from "@emotion/styled";

import Tab from "@mui/material/Tab";
import { FormControl } from "@mui/material";


export const TableContentArea = styled.div`
  @media (max-width: 800px) {
    display: none;
  }
`;

export const Container = styled.div`
  padding: 0rem;
  background-color: #e5e5e5;
  height: 100%;
`;

export const SideBar = styled.div`
  width: 150px;
  background-color: #fff;
  @media (max-width: 801px) {
    display: none;
  }
`;

export const HomeBody = styled.div`
  display: flex;
  justify-content: flex-start;
`;

export const TabArea = styled(Tab)`
  color: rgba(0, 0, 0, 0.6);
  &&.Mui-selected {
    color: black;
  }
`;

export const Title = styled.div`
  display: flex;
  align-self: flex-start;
  font-family: Poppins;
  font-weight: 600;
  font-size: 32px;
  line-height: 32px;
  margin: 24px 0 24px 0;
`;

export const MainContent = styled.main`
  margin: 0 auto;
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: column;
`;

export const TableArea = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
`;

export const FormControlArea = styled(FormControl)`
  width: 100%;
  @media (min-width: 801px) {
    display: none;
  }
  
`;

export const FooterArea = styled.div`
  display: flex;
  border-top: 1px solid #eaeaea;
  justify-content: center;
  align-items: center;
  margin: 20px 
`;

import styled from '@emotion/styled';

import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import Accordion from '@mui/material/Accordion';
import Image from 'next/image';

export const PositionsArea = styled.div`
  width: 100%;
  @media (max-width: 801px) {
    display: none;
  }
`;

export const AccordionArea = styled(Accordion)`
  width: 100%;
  margin: 0 auto;
  @media (min-width: 801px) {
    display: none;
  }
`;

export const PositionsTable = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center ;

  
`;

export const Title = styled.div`
  font-family: Poppins;
  font-weight: 600;
  font-size: 32px;
  line-height: 32px;
  margin: 24px 0;
`;

export const PositionsFields = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  padding: 0 16px;

  border: 1px solid #cad6d1;
  box-sizing: border-box;
  border-radius: 8px 8px 0px 0px;
  font-family: Poppins;
  font-style: normal;
  font-weight: 600;
  font-size: 12px;

  div {
    display: flex;
    justify-content: flex-start;
    align-items: center;
  }
`;

export const PositionEdit = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  padding: 0 16px;

  border-bottom: 1px solid #cad6d1;
  box-sizing: border-box;
  border-radius: 8px 8px 0px 0px;
  font-family: Poppins;
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  color: #587169;

  div {
    display: flex;
    justify-content: flex-start;
    align-items: center;
    text-transform: capitalize;

    &:nth-of-type(1) {
      margin: 0;
      display: flex;
      justify-content: flex-start;
      align-items: center;
      width: 100%;

      span {
        margin-left: 10px;
      }
    }

    &:nth-of-type(4) span {
      background-color: #b5f1dd;
      padding: 0 10px;
      border-radius: 10px;
    }
  }
`;

export const ImageEmployee = styled(Image)`
  border-radius: 50%;
  border: 10px solid green;
  margin-left: 200px;
  width: 50%;
`;

export const Pagination = styled.div`
  width: 100%;
  height: 100px;
  display: flex;
  justify-content: flex-end;
  font-family: Poppins;
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 150%;

  div {
    display: flex;
    justify-content: flex-end;
    align-items: center;

    &:nth-of-type(1) {
      width: 30%;
    }

    &:nth-of-type(2) {
      width: 22%;
      display: flex;
      justify-content: center;
      align-items: center;
    }
  }
`;

export const LeftArrow = styled(ChevronLeftIcon)`
  border: 1px solid black;
  border-radius: 10px 0px 0px 10px;
  margin-right: 7px;
`;

export const RightArrow = styled(ChevronRightIcon)`
  border: 1px solid black;
  border-radius: 0px 10px 10px 0px;
  margin-left: 7px;
`;

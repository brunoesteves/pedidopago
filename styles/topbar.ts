import styled from "@emotion/styled";

export const TopBarDesktop = styled.div`
  display: flex;
  justify-content: space-between;
  line-height: 61px;
  width: 100%;
  padding: 0px 32px;
  background-color: #fff;
  border-bottom: 1px solid #eaefed;
  @media (max-width: 801px) {
    display: none;
  }
`;

export const TopBarMobile = styled.div`
  display: flex;
  justify-content: space-between;
  height: 61px;
  line-height: 61px;
  width: 100%;
  padding: 0px 32px;
  background-color: #fff;
  border-bottom: 1px solid #eaefed;
  @media (min-width: 801px) {
    display: none;
  }
`;

export const TopBarItemLeft = styled.div`
  border-right: 1px solid #eaefed;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 20px 0 10px;
`;

export const TopBarItemRight = styled.div`
  display: flex;
  padding: 30px 10px;
  justify-content: center;
  align-items: center;
  border-left: 1px solid #eaefed;
`;

export const MyData = styled.div`
  height: 32px;
  width: 32px;
  border-radius: 80px;
  background-color: #b5f1dd;
  line-height: 100%;
  text-align: center;
  padding-top: 7px;
  margin: 0 ;
`;

export const NameUser = styled.div`
  display: flex;
  flex-direction: column;
  font-family: Poppins;
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 100%;
  display: flex;
  align-items: left;
  text-transform: capitalize;
`;

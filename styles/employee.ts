import styled from '@emotion/styled';

import Select from '@mui/material/Select';
import Image from 'next/image';

export const EmployeeArea = styled.div`
  margin: 0 auto;
  padding: 20px 4%;
`;

export const FormArea = styled.form`
  /* background-color: red; */
  display: flex;
  flex-direction: column;
`;

export const EmployeeTop = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;

export const InputDate = styled.input`
height:45px ;
width:100% ;

`

export const Name_Email = styled.div`
  font-family: Poppins;
  font-weight: 600;
  line-height: 27px;
  display: flex;
  flex-direction: column;
  span {
    &:nth-of-type(2) {
      font-size: 14px;
    }
  }
`;

export const TitlePersonalInfo = styled.div`
  padding: 40px 0 20px 0;
  font-family: Poppins;
  font-style: normal;
  font-weight: 600;
  font-size: 18px;
  line-height: 150%;
`;

export const PersonalInfo = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: flex-start;

  @media (max-width: 801px) {
    flex-direction: column;
  }
`;

export const Information = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  border: 2px solid #cad6d1;
  background-color: #f5faf8;
  border-radius: 10px;
  padding: 5px 12px 5px 10px;
  width: 33%;
  margin: 0 10px;

  @media (max-width: 801px) {
    width: 100%;
    margin: 10px 0;
    padding: 15px 0 15px 10px;
  }

  div {
    display: flex;
    width: 100%;
    justify-content: space-around;
    align-items: center;
  }
`;

export const TextFieldSelect = styled.div`
  margin-top: 20px;
  padding: 10px 0;
  width: 100% !important;
  border: 2px solid #cad6d1;
  border-radius: 10px;
  padding-left: 10px;
  text-align: center;
`;

export const TitleSelect = styled.div`
  font-family: Poppins;
  font-style: normal;
  font-weight: 600;
  font-size: 18px;
  line-height: 150%;
  display: flex;
  align-self: start;
  margin-bottom: 10px;
`;

export const SelectdArea = styled.select`
  width: 97%;
  height: 80px ;
  border-radius: 20px;
  font-size: 25px ;
  text-align: "flex-end" ;
  
  option{
    font-size: 15px ;
    background-color:white ;
    border-color: white ;
    text-align:"center" ;
    }
  @media (max-width: 801px) {
    width: 100%;
  }

  background-color: #f5faf8;
  & label.Mui-focused {
  }
  & .MuiInput-underline:after {
    border-bottom-color: white;
  }
  & .MuiOutlinedInput-root {
    & fieldset {
      border-color: #cad6d1;
      color: red;
    }
    &:hover fieldset {
      border-color: white;
    }
    &.Mui-focused fieldset {
      border-color: white;
    }
  }
`;

export const Title = styled.div`
  display: flex;
  align-self: flex-start;
  font-family: Poppins;
  font-weight: 600;
  font-size: 32px;
  line-height: 32px;
  margin: 24px 0;
`;

export const EmployeesFields = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  padding: 0 16px;

  border: 1px solid #cad6d1;
  box-sizing: border-box;
  border-radius: 8px 8px 0px 0px;
  font-family: Poppins;
  font-style: normal;
  font-weight: 600;
  font-size: 12px;

  div {
    display: flex;
    justify-content: flex-start;
    align-items: center;
  }
`;

export const ImageEmployee = styled(Image)`
  border-radius: 50%;
  border: 10px solid green;
  margin-left: 200px;
  width: 50%;
`;

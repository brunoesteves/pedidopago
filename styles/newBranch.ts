import styled from '@emotion/styled';

import Button from '@mui/material/Button';


export const NewBranchArea = styled.div`
  display:flex ;
  justify-content: center ;
  padding-top: 15px ;
  `;

export const FormArea = styled.form`
  /* background-color: red; */
  display: flex;
  flex-direction: column;
  width: 30%;
  `;
  
  export const ButtonArea = styled(Button)`
    margin-bottom: 20px !important;
    margin-top: 20px !important;
    width:200px ;
    display:flex ;
    align-self:center ;
  `;

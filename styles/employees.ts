import styled from '@emotion/styled';

import Image from 'next/image';

export const EmployeesArea = styled.div`
  width: 100%;
  @media (max-width: 801px) {
    display: none;
  }
`;

export const EmployeesTable = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center ;

  div {
    width: 100%;
    height: 49px;
  }
`;

export const Title = styled.div`
  display: flex;
  align-self: flex-start;
  font-family: Poppins;
  font-weight: 600;
  font-size: 32px;
  line-height: 32px;
  margin: 24px 0;
`;


export const ImageEmployee = styled(Image)`
  border-radius: 50%;
  border: 10px solid green;
  margin-left: 200px;
  width: 50%;
`;


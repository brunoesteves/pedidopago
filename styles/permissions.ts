import styled from "@emotion/styled";

import TextField from "@mui/material/TextField";
import Image from "next/image";

export const PermissionArea = styled.div`
  width: 100%;
  padding: 20px;
`;


export const TextFieldArea = styled(TextField)`

width: 47%;


 @media (max-width: 1008px) {
    width: 100%;

  }
`;

export const PermissionsTable = styled.div`
  width: 100%;

  div {
    width: 100%;
    height: 49px;
  }
`;

export const Title = styled.div`
  display: flex;
  align-self: flex-start;
  font-family: Poppins;
  font-weight: 600;
  font-size: 32px;
  line-height: 32px;
  margin: 24px 0;
`;

export const PermisssionFields = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  border: 1px solid #cad6d1;
  box-sizing: border-box;
  border-radius: 8px 8px 0px 0px;
  font-family: Poppins;
  font-style: normal;
  font-weight: 600;
  font-size: 12px;

  div {
    display: flex;
    align-items: center;
    justify-content: center;
    &:nth-of-type(1) {
      display: flex;
      justify-content: flex-start;
      align-items: center;
      padding-left: 20px;
    }
    &:nth-of-type(2) {
      display: flex;
      justify-content: flex-start;
      align-items: center;
      width: 100%;
    }
    &:nth-of-type(3) {
      display: flex;
      justify-content: flex-start;
      align-items: center;
    }
  }
`;

export const PermissionEdit = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  padding: 0 16px;

  border-bottom: 1px solid #cad6d1;
  box-sizing: border-box;
  border-radius: 8px 8px 0px 0px;
  font-family: Poppins;
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  color: #587169;

  div {
    display: flex;
    justify-content: flex-start;
    margin-left: 10px;
    align-items: center;
    text-transform: capitalize;

    &:nth-of-type(1) {
      margin: 0;

      display: flex;
      justify-content: flex-start;
      align-items: center;
      width: 100%;

      span {
        margin-left: 10px;
      }
    }
  }
`;

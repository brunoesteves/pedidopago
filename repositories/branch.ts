import { Branch } from '@prisma/client';

import prisma from './prismaClient';

const { branch: db } = prisma;

export const branchList = (): Promise<Branch[]> => db.findMany();
export const createBranch = (data: Branch): Promise<Branch> => {
  return db.create({ data });
};

export const deletebranch = (id: number): Promise<Branch> => {
  return db.delete({ where: { id } });
};

export const getUniquebranch = (id: number): Promise<Branch | null> => {
    
  return db.findUnique({ where: { id } });
};

export const updateBranch = (id: number, data: Branch): Promise<Branch> => {
  return db.update({ where: { id }, data });
};

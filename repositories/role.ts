import { Role } from '@prisma/client';
import { IRole } from '../types/roles';

import prisma from './prismaClient';

const { role: db } = prisma;

export const roleList = (): Promise<Role[]> => db.findMany();
export const deleteRole = (id: number) => db.delete({ where: { id } });
export const addRole = (newRole: string): Promise<IRole> => {

  return db.create({
    data: {
      name: newRole
    }
  });
};

import { Employee } from '@prisma/client';

import prisma from './prismaClient';

const { employee: db } = prisma;

export const employeeList = (): Promise<Employee[]> =>
  db.findMany({
    include: {
      branch: {
        select: {
          name: true
        }
      },
      role: { select: { name: true } },
      department: { select: { name: true } }
    }
  });

export const employeeInfo = (id: number) =>
  db.findUnique({
    where: { id }
  });

export const deleteEmployee = (id: number) =>
  db.delete({
    where: { id }
  });

export const updateEmployee = (id: number, data: Employee): Promise<Employee> => {

  return db.update({
    where: { id },
    data: {
      name: data.name,
      email: data.email,
      address: data.address,
      phone: data.phone,
      cpf: data.cpf,
      birth_date: data.birth_date,
      departmentID: Number(data.departmentID),
      roleID: Number(data.roleID),
      branchID: Number(data.branchID)
    }
  });
};

export const newEmployee = (data: Employee): Promise<Employee> => {
  // console.log("repo-: ", data)

  return db.create({
    data: {
      id: data.id,
      name: data.name,
      address:"",
      email: data.email,
      phone: data.phone.toString(),
      cpf: data.cpf.toString(),
      birth_date: new Date(data.birth_date),
      departmentID: Number(data.departmentID),
      roleID: Number(data.roleID),
      branchID: Number(data.branchID),
      status: data.status
    }
  });
};

import { Department } from '@prisma/client';
import { IDepartment } from '../types/departments';
import prisma from './prismaClient';

const { department: db } = prisma;

export const departmentistList = (): Promise<Department[]> => db.findMany();
export const deleteDepartment = (id: number) => db.delete({ where: { id } });
export const addRole = (newRole: string): Promise<IDepartment> => {

  return db.create({
    data: {
      name: newRole
    }
  });
};
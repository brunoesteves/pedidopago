/* eslint-disable @typescript-eslint/no-empty-function */
import { createContext, useState, useContext, ReactNode } from 'react';

interface UserContextProps {
  children: ReactNode;
}

type EmployeeContextType = {
  name: string;
  setName: (newState: string) => void;
  role: string
  setRole: (newState: string) => void;
};

const intialValue = {
  name: '',
  setName: () => {},
  role: "",
  setRole: () => {},
};
export const EmployeeContext = createContext<EmployeeContextType>(intialValue);
export const UserContextProvider = ({ children }: UserContextProps) => {
  const [name, setName] = useState(intialValue.name);
  const [role, setRole] = useState(intialValue.name);
  return <EmployeeContext.Provider value={{ name, setName, role, setRole }}>{children}</EmployeeContext.Provider>;
};

export type IBranch = {
  id: number;
  name: string;
  address: string;
  email: string;
  city: string;
  state: string;
  phone: string;
};

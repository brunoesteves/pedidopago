export type IEmployee = {
  id: number;
  name: string;
  cpf: string;
  email: string;
  address: string;
  birth_date: string;
  status: 'active' | 'inactive';
  phone: string;
  branchID: number;
  roleID: number;
  departmentID: number;
  department: {
    name: string;
  };
  role: { name: string };
  branch: { name: string };
  image: string
};
